angular.module('TecnicaTM').service('TecnicaTM_Service_Post',[
        '$q', 
        '$http', 
        'APIROUTE',
    function(
        $q, 
        $http, 
        APIROUTE
    ){
        this.getPost = function(_id){

            var defered = $q.defer();
            var promise = defered.promise;

            $http({
                method: 'GET',
                url: APIROUTE + 'posts/' + _id
            }).then(function successCallback(response) {
                defered.resolve(response.data);
            }, function errorCallback(response) {
               defered.reject(response);
            });

            return promise;
        };
    }
]);
